#ifndef KEYBOARDTEST_H
#define KEYBOARDTEST_H

#include <QMainWindow>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QKeyEvent>
#include <QProcess>
#include <QTime>
#include <QDateTime>
#include <QDir>
#include <math.h>
namespace Ui {
class keyboardTest;
}

class keyboardTest : public QMainWindow
{
    Q_OBJECT

public:
    explicit keyboardTest(QWidget *parent = 0);
    ~keyboardTest();

private slots:
    void on_pushButton_clicked();

private:
    Ui::keyboardTest *ui;
    void startCreate();
    //void outputText();

    int count = 0;
    int countError;
    int countInput;
    int countOutput;
    int simplicity;
    int duration;
    int j;
    QString textFile;
    QStringList paragraphs;
    QString inputText;
    QString outputText;
    QString diff =  "diff ./input ./output";
    QString timeStamp;
    QTime timeStart;
    QTime timeFinish;
    QProcess *proc;
    QString fileName;
    QString pathReport;
};

#endif // KEYBOARDTEST_H
