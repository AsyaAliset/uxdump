#-------------------------------------------------
#
# Project created by QtCreator 2016-03-10T11:47:23
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = databaseManager
TEMPLATE = app


SOURCES += main.cpp\
        databasemanager.cpp

HEADERS  += databasemanager.h

FORMS    += databasemanager.ui

win32:CONFIG(release, debug|release): LIBS += -lDataBaseWork
else:win32:CONFIG(debug, debug|release): LIBS += -lDataBaseWork
else:unix: LIBS += -lDataBaseWork

win32:CONFIG(release, debug|release): LIBS += -lloaderCSV
else:win32:CONFIG(debug, debug|release): LIBS += -lloaderCSV
else:unix: LIBS += -lloaderCSV
