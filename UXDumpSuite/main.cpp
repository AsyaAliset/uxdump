#include "uxdumpsuite.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QPixmap pixmap(":/splash_ok"); //Insert your splash page image here
    QSplashScreen splash(pixmap);
    splash.show();

    qApp->processEvents();//This is used to accept a click on the screen so that user can cancel the screen

    UXDumpSuite w;
    QThread::sleep(5);
    w.show();

    return a.exec();
}
